<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S05 ACTIVITY</title>
</head>
<body>
	<?php session_start(); ?>

	<?php if (!isset($_SESSION['login']) || $_SESSION['login'][0] === false): ?>

	        <form method="POST" action="./server.php" style="display:inline-block;">
	            <input type="hidden" name="action" value="login"/>
	            <label>Username: </label> <input type="text" name="username" required autocomplete="off" />
	            <label>Password: </label><input type="password" name="password" required autocomplete="off"  />
	            <button type="submit">Login</button>
	        </form>
	        <br/>
	        	<?php if(isset($_SESSION['login']) && $_SESSION['login'][0] === false) : ?>
	            	<label>Username or password incorrect.</label>
	        	<?php endif; ?>


	    <?php else: ?>

	        <p>Email: <?= $_SESSION['login'][1]; ?></p>
	        <form method="POST" action="./server.php">
	            <input type="hidden" name="action" value="logout"/>
	            <button type="submit">Logout</button>
	        </form>

	    <?php endif; ?>
	
</body>
</html>